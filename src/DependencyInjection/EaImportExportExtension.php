<?php

namespace TheoD02\EaImportExport\DependencyInjection;

use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class EaImportExportExtension extends Extension
{

    public function load(
        array            $configs,
        ContainerBuilder $container
    ) {
        $configDir = new FileLocator(__DIR__ . '/../../config');

        $loader = new YamlFileLoader($container, $configDir);
        $loader->load('services.yaml');

        $configuration = new ConfigSchema();
        $config = $this->processConfiguration($configuration, $configs);
    }
}