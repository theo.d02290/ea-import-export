<?php

namespace TheoD02\EaImportExport\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use TheoD02\EaImportExport\Entity\AdminImportExportConfig;
use TheoD02\EaImportExport\Service\ExportService;

class ExportController extends AbstractController
{
    public function __construct(
        private ExportService          $exportService,
        private EntityManagerInterface $em
    ) {}

    #[Route('/export', name: 'export')]
    public function export(Request $request): Response
    {
        $httpQuery = parse_url($request->query->get('referrer'));
        parse_str($httpQuery['query'], $query);
        $crudControllerFqcn = $query['crudControllerFqcn'];
        $entityFqcn = $crudControllerFqcn::getEntityFqcn();
        $exportConfig = $this->em->getRepository(AdminImportExportConfig::class)->findOneBy(['entity' => $entityFqcn]);
        if (null === $exportConfig) {
            $this->addFlash('danger', 'Aucune configuration d\'export n\'a été trouvée pour cette entité.');
            return $this->redirect($request->query->get('referrer'));
        }

        $response = new StreamedResponse(
            function () use
            (
                $exportConfig
            ) {
                $csv = fopen('php://output', 'wb+');
                $data = $this->exportService->export($exportConfig);
                $firstRow = true;
                foreach ($data as $row) {
                    if ($firstRow) {
                        fputcsv($csv, array_keys($row));
                        $firstRow = false;
                    }
                    fputcsv($csv, $row);
                }
                fclose($csv);
            }
        );

        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Cache-Control', 'no-store');
        $response->headers->set('X-Accel-Buffering', 'no');
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            sprintf('%s.csv', $exportConfig->getExportFilename())
        );
        $response->headers->set('Content-Disposition', $disposition);
        return $response->send();
    }
}