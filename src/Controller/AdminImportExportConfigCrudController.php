<?php

namespace TheoD02\EaImportExport\Controller;

use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TheoD02\EaImportExport\Admin\Field\ExportField;
use TheoD02\EaImportExport\Entity\AdminImportExportConfig;

class AdminImportExportConfigCrudController extends AbstractCrudController
{
    public function __construct(
        private EntityManagerInterface $em,
        private AdminUrlGenerator      $adminUrlGenerator
    ) {}

    public static function getEntityFqcn(): string
    {
        return AdminImportExportConfig::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
                     ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
    }

    public function getRedirectResponseAfterSave(
        AdminContext $context,
        string       $action
    ): RedirectResponse {
        return $this->redirect(
            $this->adminUrlGenerator
                ->setController(__CLASS__)
                ->setAction(Action::EDIT)
                ->setEntityId($context->getEntity()->getInstance()->getId())
                ->generateUrl()
        );
    }

    private function getEntitiesNames(): array
    {
        $entities = $this->em->getMetadataFactory()->getAllMetadata();
        $entitiesNames = [];
        foreach ($entities as $entity) {
            $entityName = explode('\\', $entity->getName());
            $entityName = end($entityName);
            $entitiesNames[$entityName] = $entity->getName();
        }
        return $entitiesNames;
    }

    public function getEntityFields(string $entityName): array
    {
        $entityFields = [];
        $classMetadata = $this->em->getClassMetadata($entityName);
        foreach ($classMetadata->getFieldNames() as $fieldName) {
            $entityFields['entityFields'][] = ['fieldName' => $fieldName, 'exportName' => $fieldName, 'isActive' => false, 'type' => $classMetadata->getFieldMapping(
                $fieldName
            )['type']];
        }

        $associationMappings = $classMetadata->getAssociationMappings();
        foreach ($associationMappings as $associationMapping) {
            $entityFields['entityAssociations'][$associationMapping['targetEntity']] = ['fieldName' => $associationMapping['fieldName'], 'exportName' => $associationMapping['fieldName'], 'isActive' => false, 'type' => $associationMapping['type']];
        }

        return $entityFields;
    }

    public function configureFields(string $pageName): iterable
    {
        /** @var AdminImportExportConfig $entityInstance */
        $entityInstance = $this->getContext()?->getEntity()?->getInstance();
        $entity = ChoiceField::new('entity', 'Entité')
                             ->setChoices($this->getEntitiesNames())
        ;
        $fields = ExportField::new('fields');
        if ($entityInstance) {
            $fields->setEntityFqcn($entityInstance::class);
        }

        /*$fields = CollectionField::new('fields', 'Champs')
                                 ->setEntryType(EntityFieldImportExportType::class)
                                 ->setEntryIsComplex()
                                 ->allowAdd(false)
                                 ->allowDelete(false)
                                 ->setFormTypeOption('prototype_name', 'field')
                                 ->setFormTypeOption('entry_options', ['entityFqcn' => $entityInstance?->getEntity()])
        ;
        $assocFields = CollectionField::new('fields', 'Champs')
                                      ->setEntryType(EntityFieldImportExportType::class)
                                      ->setEntryIsComplex()
                                      ->allowAdd(false)
                                      ->allowDelete(false)
                                      ->setFormTypeOption('prototype_name', 'field')
                                      ->setFormTypeOption('entry_options', ['entityFqcn' => $entityInstance?->getEntity()])
        ;*/
        /*if ($entityInstance?->getEntity() && empty($entityInstance->getFields())) {
            $fields->setFormTypeOption('data', $this->getEntityFields($entityInstance->getEntity())['entityFields']);
            $assocFields->setFormTypeOption('data', $this->getEntityFields($entityInstance->getEntity())['entityAssociations']);
        }*/

        $exportFilename = TextField::new('exportFilename', 'Nom du fichier exporté');

        return [
            $entity,
            $exportFilename,
            $fields
        ];
    }
}
