<?php

namespace TheoD02\EaImportExport\Entity;

use App\Repository\AdminImportExportConfigRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(repositoryClass: AdminImportExportConfigRepository::class),
    ORM\Table(name: 'admin_import_export_config')
]
class AdminImportExportConfig
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $entity = null;

    #[ORM\Column(type: Types::OBJECT)]
    private array $fields = [];

    #[ORM\Column(length: 255)]
    private ?string $exportFilename = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getFields(bool $onlyActive = false): array
    {
        if ($onlyActive) {
            return array_filter($this->fields, static fn($field) => $field['isActive']);
        }
        return $this->fields;
    }

    public function setFields(array $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    public function getExportFilename(): ?string
    {
        return $this->exportFilename;
    }

    public function setExportFilename(string $exportFilename): self
    {
        $this->exportFilename = $exportFilename;

        return $this;
    }
}
