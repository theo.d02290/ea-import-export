<?php

namespace TheoD02\EaImportExport;

use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TheoD02\EaImportExport\DependencyInjection\EaImportExportExtension;

class EaImportExportBundle extends Bundle
{
    protected function createContainerExtension(): ?ExtensionInterface
    {
        return new EaImportExportExtension();
    }
}