<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TheoD02\EaImportExport\Entity\AdminImportExportConfig;

/**
 * @extends ServiceEntityRepository<AdminImportExportConfig>
 *
 * @method AdminImportExportConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminImportExportConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminImportExportConfig[]    findAll()
 * @method AdminImportExportConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminImportExportConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminImportExportConfig::class);
    }

    public function add(AdminImportExportConfig $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AdminImportExportConfig $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return AdminImportExportConfig[] Returns an array of AdminImportExportConfig objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AdminImportExportConfig
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
