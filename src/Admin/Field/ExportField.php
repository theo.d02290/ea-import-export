<?php

namespace TheoD02\EaImportExport\Admin\Field;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;
use TheoD02\EaImportExport\Form\AdminExportType;

class ExportField implements FieldInterface
{
    use FieldTrait;

    public static function new(
        string  $propertyName,
        ?string $label = null
    ): ExportField {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(AdminExportType::class)
            ->setTemplatePath('bundles/EasyAdminBundle/field/export_type.html.twig')
        ;
    }

    public function setEntityFqcn(string $entityFqcn): void
    {
        $this->setFormTypeOption('entityFqcn', $entityFqcn);
    }
}