<?php

namespace TheoD02\EaImportExport\Admin\Action;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;

class ExportAction
{
    public static function get(): Action
    {
        return Action::new('export', 'Export', 'fas fa-file-export')
                     ->linkToRoute('export')
                     ->createAsGlobalAction()
        ;
    }
}