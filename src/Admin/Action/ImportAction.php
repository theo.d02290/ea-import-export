<?php

namespace TheoD02\EaImportExport\Admin\Action;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;

class ImportAction
{
    public static function get(): Action
    {
        return Action::new('import', 'Import', 'fas fa-file-import')
                     ->linkToCrudAction('import')
                     ->createAsGlobalAction()
        ;
    }
}