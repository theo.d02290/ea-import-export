<?php

namespace TheoD02\EaImportExport\Service;

use Doctrine\ORM\EntityManagerInterface;
use TheoD02\EaImportExport\Entity\AdminImportExportConfig;

class ExportService
{
    private const DATETIME_FORMAT = '%Y-%m-%d %H:%i:%S';
    private const DATE_FORMAT = '%Y-%m-%d';
    private const TIME_FORMAT = '%H:%i:%S';

    public function __construct(private EntityManagerInterface $em) {}

    public function export(AdminImportExportConfig $exportConfig)
    {
        $fields = $exportConfig->getFields(true);

        $qb = $this->em
            ->createQueryBuilder()
            ->from($exportConfig->getEntity(), 'e')
        ;

        $select = [];
        foreach ($fields as $field) {
            if (in_array($field['type'], ['datetime', 'datetime_immutable'], true)) {
                $select[] = sprintf('DATE_FORMAT(e.%s, \'%s\') as %s', $field['fieldName'], self::DATETIME_FORMAT, $field['exportName']);
            } else if (in_array($field['type'], ['date', 'date_immutable'], true)) {
                $select[] = sprintf('DATE_FORMAT(e.%s, \'%s\') as %s', $field['fieldName'], self::DATE_FORMAT, $field['exportName']);
            } else if (in_array($field['type'], ['time', 'time_immutable'], true)) {
                $select[] = sprintf('DATE_FORMAT(e.%s, \'%s\') as %s', $field['fieldName'], self::TIME_FORMAT, $field['exportName']);
            } else {
                $select[] = sprintf('e.%s as %s', $field['fieldName'], $field['exportName']);
            }
        }
        $qb->select(...$select);

        return $qb->getQuery()->toIterable();
    }
}