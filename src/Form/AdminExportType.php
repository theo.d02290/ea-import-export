<?php

namespace TheoD02\EaImportExport\Form;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminExportType extends AbstractType
{
    public function __construct(private EntityManagerInterface $em) {}

    public function buildForm(
        FormBuilderInterface $builder,
        array                $options
    ) {
        $builder->add(
            'basicFields',
            CollectionType::class,
            [
                'entry_type' => EntityFieldImportExportType::class,
                'data' => $this->getEntityFields($options['entityFqcn']),
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'entityFqcn' => null,
            ]
        );
    }

    public function getParent(): string
    {
        return CollectionType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'export_type';
    }

    public function getEntityFields(
        string $entityName,
        bool   $getAssociation = true
    ): array {
        $entityFields = [];
        $classMetadata = $this->em->getClassMetadata($entityName);
        foreach ($classMetadata->getFieldNames() as $fieldName) {
            $entityFields['entityFields'][] = ['fieldName' => $fieldName, 'exportName' => $fieldName, 'isActive' => false, 'type' => $classMetadata->getFieldMapping(
                $fieldName
            )['type']];
        }

        $associationMappings = $classMetadata->getAssociationMappings();
        if ($getAssociation) {
            foreach ($associationMappings as $associationMapping) {
                $entityFields['entityAssociations'][$associationMapping['targetEntity']] = ['fieldName' => $associationMapping['fieldName'], 'exportName' => $associationMapping['fieldName'], 'isActive' => false, 'type' => $associationMapping['type']];
                if (in_array($associationMapping['type'], [ClassMetadataInfo::MANY_TO_MANY, ClassMetadataInfo::MANY_TO_ONE], true)) {
                    $entityFields['entityAssociations'][$associationMapping['targetEntity']]['fields'] = $this->getEntityFields($associationMapping['targetEntity']);
                } else {
                    $entityFields['entityAssociations'][$associationMapping['targetEntity']]['fields'] = $this->getEntityFields($associationMapping['targetEntity'], false);
                }
            }
        }

        return $entityFields;
    }
}