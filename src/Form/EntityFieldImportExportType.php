<?php

namespace TheoD02\EaImportExport\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EntityFieldImportExportType extends AbstractType
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {}

    public function buildForm(
        FormBuilderInterface $builder,
        array                $options
    ): void {
        $builder
            ->add(
                'fieldName',
                TextType::class,
                [
                    'label' => 'Nom du champ de base',
                    'disabled' => true,
                ]
            )
            ->add(
                'exportName',
                TextType::class,
                [
                    'label' => 'Nom du champ export/import',
                ]
            )
            ->add(
                'isActive',
                CheckboxType::class,
                [
                    'label' => 'Actif',
                    'required' => false,
                ]
            )
        ;
    }
}
